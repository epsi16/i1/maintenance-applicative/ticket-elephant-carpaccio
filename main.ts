import * as readline from 'readline';

let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


console.log("Hello World");


console.log("--------------------\n" +
  "TAXES\n" +
  "--------------------\n" +
  "UT\t6.85%\n" +
  "FR\t20.00%\n" +
  "NV\t8.00%\n" +
  "TX\t6.25%\n" +
  "AL\t4.00%\n" +
  "CA\t8.25%\n" +
  "-----------------------"
);

var HT: number;
var TVA: number;
var TTC: number;

rl.question('Montant hors taxe :', (answer) => {
  HT = parseInt(answer);

  rl.question('TVA :', (answer) => {
    TVA = parseInt(answer);

    TTC = HT * (TVA / 100) + HT;
    console.log("Prix TTC : " + TTC);

    rl.close();
  });
});




